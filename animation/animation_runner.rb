# frozen_string_literal: true

class AnimationRunner
  attr_reader :algorithm, :formatter

  def initialize(algorithm, formatter, options = {})
    @algorithm = algorithm
    @formatter = formatter
    @fps = options[:fps] || 5
    @title = options[:title] || ''
  end

  def start_animation
    print "#{@title}\n\n"
    formatter.reset_events!
    algorithm.add_observer(self, :print_updated)
    algorithm.run
    puts formatter.format
  end

  def print_updated(event)
    formatter.add_event(event)
    puts formatter.format
    sleep 1.0 / @fps

    puts formatter.restart
  end
end
