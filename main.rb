# frozen_string_literal: true

class Main
  def initialize(options)
    @options = {
      width: 5,
      height: 5,
      fps: 10,
      create_with: :recursive,
      traverse_with: :bfs
    }.merge(options.reject { |_, value| value.nil? })
  end

  def start
    maze = Maze.new(options[:width].to_i, options[:height].to_i)
    formatter = Formatter::Text.new(maze)
    fps = options[:fps].to_f

    title = "1.Creating the maze with *** #{creator[:name]} ***"
    creator_algorithm = creator[:algorithm_class].new(maze)
    AnimationRunner.new(creator_algorithm, formatter, title: title, fps: fps).start_animation

    title = "2.Checking if the cells are accessible with *** #{traversal[:name]} ***"
    traversal_algorithm = traversal[:algorithm_class].new(maze)
    AnimationRunner.new(traversal_algorithm, formatter, title: title, fps: fps).start_animation

    title = '3.Finding the shortest path with *** A* ***'
    finder = Finder::AStar.new(maze)
    AnimationRunner.new(finder, formatter, title: title, fps: fps).start_animation
  end

  private

  attr_reader :options

  def creator
    creator_option = options[:create_with]

    case creator_option.to_sym
    when :recursive
      {
        algorithm_class: Creator::RecursiveBacktracker,
        name: 'Recursive Backtracking'
      }
    when :binary
      {
        algorithm_class: Creator::BinaryTree,
        name: 'Binary Tree'
      }
    else
      raise ArgumentError, 'Please, use recursive or binary'
    end
  end

  def traversal
    traversal_option = options[:traverse_with]

    case traversal_option.to_sym
    when :bfs
      {
        algorithm_class: Traversal::Bfs,
        name: 'BFS'
      }
    when :dfs
      {
        algorithm_class: Traversal::Bfs,
        name: 'DFS'
      }
    else
      raise ArgumentError, 'Please, use bfs or dfs'
    end
  end
end
