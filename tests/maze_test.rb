# frozen_string_literal: true

require './test_helper'

class MazeTest < MiniTest::Test
  def test_returns_correct_cell_when_visiting_direction
    maze = Maze.new
    start_cell = maze.start_cell
    assert start_cell.start?

    new_cell = maze.visit(start_cell, :south)
    assert_equal new_cell.x, 0
    assert_equal new_cell.y, 1
    assert !new_cell.start?

    new_cell = maze.visit(new_cell, :south)
    assert_equal new_cell.x, 0
    assert_equal new_cell.y, 2

    new_cell = maze.visit(new_cell, :east)
    assert_equal new_cell.x, 1
    assert_equal new_cell.y, 2
  end

  def test_stores_correct_cell
    maze = Maze.new
    start_cell = maze.start_cell
    maze.visit(start_cell, :south)

    cells = maze.neighbors(start_cell)
    assert_equal cells.length, 1
    assert_equal cells[0].x, 0
    assert_equal cells[0].y, 1
  end

  def test_does_not_store_out_of_boundary_cells
    maze = Maze.new
    start_cell = maze.start_cell

    cell = maze.visit(start_cell, :north)
    assert_nil cell

    cell = maze.visit(start_cell, :west)
    assert_nil cell

    neighbors = maze.neighbors(start_cell)
    assert_equal neighbors, []
  end

  def test_add_neighbor_to_affected_neighbor
    maze = Maze.new

    start_cell = maze.start_cell
    new_cell = maze.visit(start_cell, :south)

    assert_equal maze.neighbors(new_cell).length, 1
    assert_equal maze.neighbors(new_cell)[0].y, 0

    cell = maze.visit(new_cell, :north)

    assert_nil cell
  end

  def test_does_not_store_out_of_boundary_cells_from_destination
    maze = Maze.new
    destination_cell = maze.destination_cell
    destination_cell.destination?

    cell = maze.visit(destination_cell, :south)
    assert_nil cell

    cell = maze.visit(destination_cell, :south)
    assert_nil cell
  end

  def test_returns_random_cell
    maze = Maze.new(3, 2)
    cell = maze.random_cell
    assert_instance_of Cell, cell
    assert_includes (0..2), cell.x
    assert_includes (0..1), cell.y
  end

  def test_cell_at_returns_the_correct_cell
    maze = Maze.new(3, 3)

    new_cell = maze.cell_at(1, 1)
    maze.visit(new_cell, :east)

    new_cell = maze.cell_at(1, 1)
    neighbors = maze.neighbors(new_cell)

    assert_equal neighbors.length, 1
    assert_equal neighbors[0].x, 2
    assert_equal neighbors[0].y, 1
  end

  def test_visits_random_visits_randomly
    maze = Maze.new(3, 3)
    cell = maze.start_cell
    new_cell = maze.visit_random(cell, %i[south])
    assert_equal new_cell.x, 0
    assert_equal new_cell.y, 1
    assert_equal maze.neighbors(new_cell).length, 1

    new_cell = maze.visit_random(cell, %i[east south])
    assert_equal new_cell.x, 1
    assert_equal new_cell.y, 0
    assert_equal maze.neighbors(new_cell).length, 1

    new_cell = maze.visit_random(cell)
    assert_nil new_cell
  end

  def test_visit_random_returns_nil_when_every_cell_is_visited
    maze = Maze.new(2, 2)
    cell = maze.random_cell
    4.times.each { maze.visit_random(cell) }
    new_cell = maze.visit_random(cell)
    assert_nil new_cell
  end

  def test_each_cell_retuns_all_cells
    maze = Maze.new(3, 3)
    cells = []
    maze.each_cell do |cell|
      cells << cell
    end

    assert_equal cells[0].x, 0
    assert_equal cells[0].y, 0

    assert_equal cells[4].x, 1
    assert_equal cells[4].y, 1

    assert_equal cells[8].y, 2
    assert_equal cells[8].y, 2
  end

  def test_visit_all
    maze = Maze.new(10, 10)
    maze.visit_all
    cell = maze.cell_at(4, 4)
    neighbors = maze.neighbors(cell)
    assert_equal neighbors.length, 4

    cell = maze.cell_at(9, 9)
    neighbors = maze.neighbors(cell)
    assert_equal neighbors.length, 2

    cell = maze.cell_at(9, 8)
    neighbors = maze.neighbors(cell)
    assert_equal neighbors.length, 3

    cell = maze.cell_at(0, 0)
    neighbors = maze.neighbors(cell)
    assert_equal neighbors.length, 2
  end

  def test_all_neighbors
    maze = Maze.new(10, 10)

    cells = maze.all_neighbors(maze.start_cell)
    assert_equal cells.length, 2
    coordinates = cells.map(&:coordinates)
    assert coordinates.include?([0, 1])
    assert coordinates.include?([1, 0])

    cells = maze.all_neighbors(maze.destination_cell)
    assert_equal cells.length, 2
    coordinates = cells.map(&:coordinates)
    assert coordinates.include?([8, 9])
    assert coordinates.include?([9, 8])

    cell = maze.cell_at(5, 5)
    cells = maze.all_neighbors(cell)
    assert_equal cells.length, 4
    coordinates = cells.map(&:coordinates)
    assert coordinates.include?([4, 5])
    assert coordinates.include?([5, 4])
  end

  def test_area_returns_correct_number
    maze = Maze.new(10, 1)
    assert_equal maze.area, 10

    maze = Maze.new(10, 5)
    assert_equal maze.area, 50

    maze = Maze.new(10, 10)
    assert_equal maze.area, 100
  end
end
