# frozen_string_literal: true

require './test_helper'

class EventEmitterTest < MiniTest::Test
  class Algorithm
    include EventEmitter

    def initialize(maze)
      @maze = maze
    end

    def notify_visit_test
      notify_visit(yield)
    end

    def notify_goal_test
      notify_goal(yield)
    end
  end

  def test_notify_visit_emits_correct_event
    maze = Maze.new(10, 10)
    cell = maze.cell_at(1, 2)
    alg = Algorithm.new(maze)

    expectation = lambda do |event|
      assert_equal event.cell, cell
      assert_equal event.type, Event::VISIT
    end

    alg.add_observer(expectation, :call)
    alg.notify_visit_test { cell }
  end

  def test_notify_goal_emits_correct_event
    maze = Maze.new(10, 10)
    cell = maze.cell_at(1, 2)
    alg = Algorithm.new(maze)

    expectation = lambda do |event|
      assert_equal event.cell, cell
      assert_equal event.type, Event::GOAL
    end

    alg.add_observer(expectation, :call)
    alg.notify_goal_test { cell }
  end
end
