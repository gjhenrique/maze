# frozen_string_literal: true

require './test_helper'

class AStar < MiniTest::Test
  def test_visit_path_without_barrier
    maze = Maze.new(10, 10)
    maze.visit_all
    algorithm = Finder::AStar.new(maze)
    path = algorithm.run
    assert_equal path.length, 19
    assert_equal path.first.coordinates, [0, 0]
    assert_equal path.last.coordinates, [9, 9]
  end

  def test_visit_path_with_some_walls
    maze = Maze.new(10, 10)

    cells = (4..5).flat_map do |i|
      10.times.map do |j|
        maze.cell_at(j, i)
      end
    end

    maze.visit_all(cells)
    maze.visit(maze.cell_at(4, 4), :south)
    algorithm = Finder::AStar.new(maze)
    path = algorithm.run

    assert_equal path.first.coordinates, [0, 0]
    assert_equal path.last.coordinates, [9, 9]
    assert_equal path.length, 19
  end

  def test_returns_empty_array_when_there_is_not_a_path
    maze = Maze.new(10, 10)

    cells = (4..5).flat_map do |i|
      10.times.map do |j|
        maze.cell_at(j, i)
      end
    end

    maze.visit_all(cells)
    algorithm = Finder::AStar.new(maze)
    path = algorithm.run

    assert_equal path, []
  end

  def test_visit_path_with_algorithm
    maze = Maze.new(10, 10)

    creator = Creator::BinaryTree.new(maze)
    creator.run

    algorithm = Finder::AStar.new(maze)
    path = algorithm.run
    assert_equal path.first.coordinates, [0, 0]
    assert_equal path.last.coordinates, [9, 9]
  end
end
