# frozen_string_literal: true

require './test_helper'

class RecursiveBacktrackerTest < MiniTest::Test
  def test_it_iterates_correctly
    maze = Maze.new(30, 30)

    algorithm = Creator::RecursiveBacktracker.new(maze)

    path = algorithm.run
    assert_equal path.length, 30 * 30
  end

  def test_it_is_accessible_by_every_node
    maze = Maze.new(30, 30)

    algorithm = Creator::RecursiveBacktracker.new(maze)
    algorithm.run

    traversal = Traversal::Dfs.new(maze)
    cells = traversal.run
    set = Set.new(cells)
    assert_equal set.length, 30 * 30
  end
end
