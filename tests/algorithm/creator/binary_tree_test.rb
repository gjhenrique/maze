# frozen_string_literal: true

require './test_helper'

class BinaryTreeTest < MiniTest::Test
  def test_it_returns_all_the_cells
    maze = Maze.new(10, 5)
    algorithm = Creator::BinaryTree.new(maze)

    cells = algorithm.run

    set = Set.new(cells)
    assert_equal set.length, 50
  end
end
