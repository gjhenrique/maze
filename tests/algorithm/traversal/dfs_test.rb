# frozen_string_literal: true

require './test_helper'

class DfsTest < MiniTest::Test
  def test_visits_single_node_for_non_connected_maze
    maze = Maze.new(2, 2)
    dfs = Traversal::Dfs.new(maze)
    array = dfs.run
    assert_equal array.length, 1
    assert_equal array.first.coordinates, [0, 0]
  end

  def test_visits_all_node_for_one_cell
    maze = Maze.new(2, 2)
    cell = maze.start_cell
    maze.visit(cell, :south)

    dfs = Traversal::Dfs.new(maze)
    array = dfs.run
    assert_equal array.length, 2
    assert_equal array[0].coordinates, [0, 0]
    assert_equal array[1].coordinates, [0, 1]
  end

  def test_visits_all_node_for_two_cells
    maze = Maze.new(2, 2)
    cell = maze.start_cell
    cell = maze.visit(cell, :south)
    maze.visit(cell, :east)

    dfs = Traversal::Dfs.new(maze)
    array = dfs.run
    assert_equal array.length, 3
    assert_equal array[0].coordinates, [0, 0]
    assert_equal array[1].coordinates, [0, 1]
    assert_equal array[2].coordinates, [1, 1]
  end

  def test_visits_all_node_for_three_cells
    maze = Maze.new(2, 2)
    cell = maze.start_cell
    cell = maze.visit(cell, :south)
    maze.visit(cell, :east)

    dfs = Traversal::Dfs.new(maze)
    array = dfs.run
    assert_equal array.length, 3
    assert_equal array[0].coordinates, [0, 0]
    assert_equal array[1].coordinates, [0, 1]
    assert_equal array[2].coordinates, [1, 1]
  end

  def test_visits_all_node_for_four_cells
    maze = Maze.new(2, 2)
    cell = maze.start_cell
    cell = maze.visit(cell, :south)
    cell = maze.visit(cell, :east)
    maze.visit(cell, :north)

    dfs = Traversal::Dfs.new(maze)
    array = dfs.run
    assert_equal array.length, 4
    assert_equal array[0].coordinates, [0, 0]
    assert_equal array[1].coordinates, [0, 1]
    assert_equal array[2].coordinates, [1, 1]
    assert_equal array[3].coordinates, [1, 0]
  end
end
