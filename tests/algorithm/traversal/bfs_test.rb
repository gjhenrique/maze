# frozen_string_literal: true

require './test_helper'

class Bfs < MiniTest::Test
  def test_visits_single_node_for_non_connected_maze
    maze = Maze.new(2, 2)
    bfs = Traversal::Bfs.new(maze)
    array = bfs.run
    assert_equal array.length, 1
    assert_equal array.first.coordinates, [0, 0]
  end

  def test_visits_all_node_for_one_cell
    maze = Maze.new(2, 2)
    cell = maze.start_cell
    maze.visit(cell, :south)

    bfs = Traversal::Bfs.new(maze)
    array = bfs.run
    assert_equal array.length, 2
    assert_equal array[0].coordinates, [0, 0]
    assert_equal array[1].coordinates, [0, 1]
  end

  def test_visits_all_node_for_two_cells
    maze = Maze.new(2, 2)
    cell = maze.start_cell
    cell = maze.visit(cell, :south)
    maze.visit(cell, :east)

    bfs = Traversal::Bfs.new(maze)
    array = bfs.run
    assert_equal array.length, 3
    assert_equal array[0].coordinates, [0, 0]
    assert_equal array[1].coordinates, [0, 1]
    assert_equal array[2].coordinates, [1, 1]
  end

  def test_visits_all_node_for_three_cells
    maze = Maze.new(2, 2)
    cell = maze.start_cell
    cell = maze.visit(cell, :south)
    maze.visit(cell, :east)

    bfs = Traversal::Bfs.new(maze)
    array = bfs.run
    assert_equal array.length, 3
    assert_equal array[0].coordinates, [0, 0]
    assert_equal array[1].coordinates, [0, 1]
    assert_equal array[2].coordinates, [1, 1]
  end

  def test_visits_all_node_for_four_cells
    maze = Maze.new(2, 2)
    cell = maze.start_cell
    cell = maze.visit(cell, :south)
    cell = maze.visit(cell, :east)
    maze.visit(cell, :north)

    bfs = Traversal::Bfs.new(maze)
    array = bfs.run
    assert_equal array.length, 4
    assert_equal array[0].coordinates, [0, 0]
    assert_equal array[1].coordinates, [0, 1]
    assert_equal array[2].coordinates, [1, 1]
    assert_equal array[3].coordinates, [1, 0]
  end

  def test_visits_all_node_when_there_is_no_wall
    maze = Maze.new(30, 30)
    maze.visit_all

    bfs = Traversal::Bfs.new(maze)
    cells = bfs.run
    assert_equal cells.length, 30 * 30
  end

  def test_does_not_visit_nodes_when_there_is_a_wall
    maze = Maze.new(30, 30)

    excluded_cells = (4..5).flat_map do |i|
      30.times.map do |j|
        maze.cell_at(j, i)
      end
    end

    maze.visit_all(excluded_cells)

    bfs = Traversal::Bfs.new(maze)
    cells = bfs.run
    assert_equal cells.length, 30 * 5
  end

  def test_visits_all_nodes
    maze = Maze.new(30, 30)

    excluded_cells = (4..5).flat_map do |i|
      30.times.map do |j|
        maze.cell_at(j, i)
      end
    end

    maze.visit_all(excluded_cells)
    maze.visit(maze.cell_at(15, 4), :south)

    bfs = Traversal::Bfs.new(maze)
    cells = bfs.run
    assert_equal cells.length, 30 * 30
  end
end
