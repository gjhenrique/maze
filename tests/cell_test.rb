# frozen_string_literal: true

require './test_helper'

class CellTest < MiniTest::Test
  def test_returns_the_correct_direction
    cell = Cell.new(10, 20)
    assert_equal cell.x, 10
    assert_equal cell.y, 20
  end

  def test_returns_the_direction_by_method
    cell = Cell.new(10, 20)
    assert_equal cell.x, 10
    assert_equal cell.y, 20
  end

  def test_raises_error_with_negative_positions
    assert_raises(ArgumentError) { Cell.new(-1, 20) }
    assert_raises(ArgumentError) { Cell.new(10, -1) }
  end

  def test_correctly_sets_start_and_destination_option
    cell = Cell.new(0, 0, start: true)
    assert cell.start?

    cell = Cell.new(0, 0, destination: true)
    assert cell.destination?

    cell = Cell.new(0, 0, destination: false, start: false)
    assert !cell.destination?
    assert !cell.start?
  end

  def test_equality
    cell1 = Cell.new(10, 10)
    cell2 = Cell.new(10, 10)
    assert_equal cell1, cell2

    cell3 = Cell.new(10, 11)
    assert cell1 != cell3

    cell4 = Cell.new(10, 11)
    assert cell1 != cell4

    assert !cell1.nil?
  end

  def test_eql?
    cell1 = Cell.new(1, 1)
    cell2 = Cell.new(1, 1)
    cell3 = Cell.new(2, 1)
    set = Set.new([cell1, cell2, cell3])
    assert_equal set.length, 2

    hash = {}
    hash[cell1] = 1
    assert_equal hash[cell2], 1
  end
end
