# frozen_string_literal: true

require 'rubygems'
require 'bundler'
require 'set'
Bundler.require(:default)

require './domain/cell'
require './domain/maze'
require './domain/store/prime_factorization_strategy'

require './events/event_emitter'
require './events/event'
require './animation/animation_runner'

Dir.glob('./algorithms/creator/*.rb') { |file| require file }
Dir.glob('./algorithms/finder/*.rb') { |file| require file }
Dir.glob('./algorithms/traversal/*.rb') { |file| require file }

Dir.glob('./formatter/*.rb') { |file| require file }

require './main'
