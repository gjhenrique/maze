# frozen_string_literal: true

module Traversal
  class Bfs
    include EventEmitter

    attr_reader :maze

    def initialize(maze)
      @maze = maze
    end

    def run(cell = maze.start_cell)
      queue = [cell]
      visited = [cell]

      until queue.empty?
        cell = queue.shift

        notify_visit(cell)
        maze.neighbors(cell).each do |neighbor|
          next if visited.include?(neighbor)

          queue.push(neighbor)
          visited.push(neighbor)
        end
      end

      visited
    end

    private

    attr_reader :stack, :visited

    def unvisited_cell?(cell)
      !visited.include?(cell)
    end
  end
end
