# frozen_string_literal: true

require 'set'

module Traversal
  class Dfs
    include EventEmitter

    attr_reader :maze

    def initialize(maze)
      @maze = maze
    end

    def run(cell = maze.start_cell, visited = [])
      visited.push(cell)

      maze.neighbors(cell).each do |neighbor|
        notify_visit(cell)
        run(neighbor, visited) unless visited.include?(neighbor)
      end

      visited
    end
  end
end
