# frozen_string_literal: true

module Creator
  class BinaryTree
    include EventEmitter

    attr_reader :maze

    def initialize(maze)
      @maze = maze
    end

    def run
      visited = []

      maze.each_cell do |cell|
        maze.visit_random(cell, %i[north west])
        visited.push(cell)
        notify_visit(cell)
      end

      visited
    end
  end
end
