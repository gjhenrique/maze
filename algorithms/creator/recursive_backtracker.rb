# frozen_string_literal: true

module Creator
  class RecursiveBacktracker
    include EventEmitter

    attr_reader :maze

    def initialize(maze)
      @maze = maze
    end

    def run(cell = maze.random_cell, visited = [], stack = [])
      neighbors = maze.all_neighbors(cell)
      unvisited_neighbors = neighbors - visited

      return visited if visited.length >= maze.area

      if unvisited_neighbors.empty?
        cell = stack.pop
        return run(cell, visited, stack)
      else
        neighbor_cell = unvisited_neighbors.sample
        maze.visit_cell(cell, neighbor_cell)
        visited.push(neighbor_cell)
        stack.push(cell)
        notify_visit(neighbor_cell)

        return run(neighbor_cell, visited, stack)
      end
    end
  end
end
