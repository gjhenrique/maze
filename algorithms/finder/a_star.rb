# frozen_string_literal: true

require 'pqueue'

module Finder
  class Node
    attr_reader :cell, :cost, :current_cost, :parent

    def ==(other)
      cell == other.cell
    end

    def initialize(cell, cost, current_cost, parent)
      @cell = cell
      @cost = cost
      @current_cost = current_cost
      @parent = parent
    end
  end

  class AStar
    include EventEmitter

    attr_reader :maze

    def initialize(maze)
      @maze = maze
    end

    def run(start_cell = maze.start_cell, end_cell = maze.destination_cell)
      open_list = PQueue.new(&method(:comparator))
      closed_list = Set.new

      node = Node.new(start_cell, manhattan(start_cell, end_cell), 0, nil)
      open_list.push(node)

      until open_list.empty?
        node = open_list.pop
        cell = node.cell
        closed_list.add(cell)

        return mount_path(node) if cell == end_cell

        maze.neighbors(cell).each do |neighbor|
          next if closed_list.include?(neighbor)

          neighbor_current_cost = node.current_cost + 1
          neighbor_total_cost = neighbor_current_cost + manhattan(neighbor, end_cell)
          new_node = Node.new(neighbor, neighbor_total_cost, neighbor_current_cost, node)

          if !open_list.include?(new_node)
            notify_visit(new_node.cell)
            open_list.push(new_node)
          elsif new_node.current_cost >= node.current_cost
            next
          end
        end
      end

      # we didn't find the end node
      []
    end

    private

    def comparator(node, other_node)
      total_cost = other_node.cost <=> node.cost
      return total_cost unless total_cost.zero?

      # We have a lot of ties, since the cost is the same to move
      # Then we untie it up with nodes that are more far away from the start cell
      node.current_cost <=> other_node.current_cost
    end

    def manhattan(cell, other)
      (cell.x - other.x).abs + (cell.y - other.y).abs
    end

    def mount_path(node, path = [])
      return path if node.nil?
      notify_goal(node.cell)
      mount_path(node.parent, path.unshift(node.cell))
    end
  end
end
