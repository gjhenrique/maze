# frozen_string_literal: true

require 'observer'

module EventEmitter
  include Observable

  def notify_visit(cell)
    changed
    notify_observers(Event.new(cell, Event::VISIT))
  end

  def notify_goal(cell)
    changed
    notify_observers(Event.new(cell, Event::GOAL))
  end
end
