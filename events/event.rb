# frozen_string_literal: true

class Event
  attr_reader :cell, :type

  VISIT = :visit
  GOAL = :goal

  def initialize(cell, type = :visit)
    @cell = cell
    @type = type
  end
end
