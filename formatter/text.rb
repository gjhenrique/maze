# frozen_string_literal: true

module Formatter
  class Text
    VERTICAL_WALL = '-'
    HORIZONTAL_WALL = '|'
    CORNER_TEXT = '+'
    EMPTY_CELL = ' '
    VISITED_CELL = '@'
    GOAL_CELL = '*'
    START_CELL = 'S'
    DESTINATION_CELL = 'F'

    def initialize(maze)
      @maze = maze
      @board_string = ''
      @events = []
    end

    def format
      reset!

      maze.height.times.each do |y|
        top_str = CORNER_TEXT.dup
        cell_str = HORIZONTAL_WALL.dup

        maze.width.times.each do |x|
          cell = maze.cell_at(x, y)
          neighbors = maze.neighbors(cell)
          top_str << calculate_top(cell, neighbors)
          cell_str << calculate_bottom(cell, neighbors)
        end

        append(top_str)
        jump_line!
        append(cell_str)
        jump_line!
      end

      append(VERTICAL_WALL + (VERTICAL_WALL + CORNER_TEXT) * maze.width)
    end

    def reset_events!
      @events = []
    end

    def restart
      "\r" + ("\e[A\e[K" * (maze.height * 2 + 2))
    end

    def add_event(event)
      events << event
    end

    private

    attr_reader :maze, :str, :events

    def reset!
      @board_string = ''
    end

    def jump_line!
      append("\n")
    end

    def append(string)
      @board_string += string
    end

    def calculate_top(cell, neighbors)
      north_neighbor = neighbors.any? { |c| c.y < cell.y }
      wall = north_neighbor ? ' ' : VERTICAL_WALL
      wall + CORNER_TEXT
    end

    def calculate_bottom(cell, neighbors)
      east_neighbor = neighbors.any? { |c| c.x > cell.x }
      wall = east_neighbor ? ' ' : HORIZONTAL_WALL
      cell_text(cell) + wall
    end

    def cell_text(cell)
      if cell_in_events?(cell, Event::GOAL)
        GOAL_CELL
      elsif cell_in_events?(cell, Event::VISIT)
        VISITED_CELL
      elsif cell.start?
        START_CELL
      elsif cell.destination?
        DESTINATION_CELL
      else
        EMPTY_CELL
      end
    end

    def cell_in_events?(cell, type)
      events.any? do |event|
        event.cell == cell && event.type == type
      end
    end
  end
end
