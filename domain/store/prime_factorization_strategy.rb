# frozen_string_literal: true

require 'matrix'

# []= is private in ruby, so let's make it public
# Our Matrix is mutable now
class Matrix
  public :"[]="
end

class PrimeFactorizationStrategy
  PRIME_DIRECTIONS = [
    {
      name: :north,
      prime_number: 2
    },
    {
      name: :south,
      prime_number: 3
    },
    {
      name: :east,
      prime_number: 5
    },
    {
      name: :west,
      prime_number: 7
    }
  ].freeze

  def initialize(width, height)
    @matrix = Matrix.build(width, height) { 1 }
  end

  def add_edge(cell, direction)
    direction = PRIME_DIRECTIONS.find { |d| direction == d[:name] }
    prime_number = direction[:prime_number]
    number = matrix[cell.x, cell.y]
    matrix[cell.x, cell.y] = number * prime_number unless neighbor?(number, prime_number)
  end

  def neighbors(cell)
    number = matrix[cell.x, cell.y]

    directions = PRIME_DIRECTIONS.select do |direction|
      neighbor?(number, direction[:prime_number])
    end

    directions.map { |direction| direction[:name] }
  end

  private

  attr_reader :matrix

  def neighbor?(number, prime_number)
    (number % prime_number).zero?
  end
end
