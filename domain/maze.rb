# frozen_string_literal: true

class Maze
  DIRECTIONS = [
    {
      name: :north,
      opposite: :south,
      new_position: ->(x, y) { [x, y - 1] }
    },
    {
      name: :south,
      opposite: :north,
      new_position: ->(x, y) { [x, y + 1] }
    },
    {
      name: :east,
      opposite: :west,
      new_position: ->(x, y) { [x + 1, y] }
    },
    {
      name: :west,
      opposite: :east,
      new_position: ->(x, y) { [x - 1, y] }
    }
  ].freeze

  attr_reader :width, :height

  def initialize(
        width = 10,
        height = 10,
        strategy = PrimeFactorizationStrategy.new(width, height)
      )
    @width = width
    @height = height
    @storage_strategy = strategy
  end

  def cell_at(x, y)
    build_cell(x, y)
  end

  def start_cell
    build_cell(0, 0)
  end

  def destination_cell
    build_cell(width - 1, height - 1)
  end

  def random_cell
    width_rand = rand(0..(width - 1))
    height_rand = rand(0..(height - 1))

    build_cell(width_rand, height_rand)
  end

  def neighbors(cell)
    directions = storage_strategy.neighbors(cell)

    directions.map do |direction|
      new_x, new_y = find_direction_name(direction)[:new_position].call(cell.x, cell.y)
      build_cell(new_x, new_y)
    end
  end

  def all_neighbors(cell)
    DIRECTIONS.map do |direction|
      new_x, new_y = direction[:new_position].call(*cell.coordinates)
      build_cell(new_x, new_y) if position_allowed?(new_x, new_y)
    end.compact
  end

  def visit_cell(cell, target_cell)
    target_direction = DIRECTIONS.find do |direction|
      target_coordinate = direction[:new_position].call(*cell.coordinates)
      target_cell.coordinates == target_coordinate
    end

    visit(cell, target_direction[:name])
  end

  def visit_random(cell, directions = DIRECTIONS.map { |d| d[:name] })
    neighbor_directions = storage_strategy.neighbors(cell)
    remaining_directions = directions - neighbor_directions

    remaining_directions.shuffle.reduce(nil) do |value, direction|
      next value unless value.nil?
      visit(cell, direction)
    end
  end

  def each_cell
    height.times.each do |y|
      width.times.each do |x|
        yield build_cell(x, y)
      end
    end
  end

  def visit_all(except_cells = [])
    each_cell do |cell|
      next if except_cells.include?(cell)

      DIRECTIONS.each do |direction|
        visit(cell, direction[:name])
      end
    end
  end

  def area
    width * height
  end

  def visit(cell, direction_name)
    direction = find_direction_name(direction_name)
    x, y = direction[:new_position].call(cell.x, cell.y)

    neighbor_directions = storage_strategy.neighbors(cell)
    return nil if !position_allowed?(x, y) || neighbor_directions.include?(direction[:name])

    build_cell(x, y).tap do |new_cell|
      storage_strategy.add_edge(cell, direction[:name])
      storage_strategy.add_edge(new_cell, direction[:opposite])
    end
  end

  private

  attr_reader :storage_strategy

  def build_cell(x, y)
    cell_options = {
      destination: x == width - 1 && y == height - 1,
      start: x.zero? && y.zero?
    }
    Cell.new(x, y, cell_options)
  end

  def find_direction_name(direction)
    DIRECTIONS.find { |d| d[:name] == direction }
  end

  def position_allowed?(x, y)
    x >= 0 && x < width && y >= 0 && y < height
  end
end
