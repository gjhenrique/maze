# frozen_string_literal: true

class Cell
  attr_reader :x, :y

  def initialize(x, y, options = {})
    raise ArgumentError if x.negative? || y.negative?
    @x = x
    @y = y
    @options = options
  end

  def start?
    @options[:start]
  end

  def destination?
    @options[:destination]
  end

  def ==(other)
    self.class == other.class && x == other.x && y == other.y
  end

  def eql?(other)
    self == other
  end

  def hash
    [x, y].hash
  end

  def coordinates
    [x, y]
  end
end
