# Installation
I used ruby to do this test and I tried to keep dependencies at a miminum. So I'm confident it'll work properly

Install the gems
```
gem install bundler
bundle install

# Execute an animation of the 3 steps (creation, traversal and pathfinding) with some default values
./maze animation
# If that fails, try to run with:
bundle exec ./maze animation
```

# Command-line arguments
 It's possible to give the animation command some parameters, like changing the width, height, fps, creation and traversal algorithm

 ```
./maze animation --width 20 --height 20 --fps 15 --create_with [binary|recursive] --traverse_with [dfs|bfs]
 ```


# Goals

### 1.creates a random maze given width and height of the board and displays it

To create the random maze, I used two algorithms:

**BinaryTree:**
Very simple algorithm that iterates over each cell of the maze and remove the wall either from north or west

To check this algorithm in action, run with `./maze animation --create_with binary`

**Recursive Backtracking:**
Choose a random cell and remove a random wall from a neighbor that was not visited yet and stores it that cell in a stack (LIFO). Do this recursively until there are no neighbors remaining yet. 
When there are no neighbors, pop from the stack and restart it from that cell.
The algorithm stops when every cell is visited

To check this algorithm in action, you don't need to pass any parameter, since it's the default.
Simply run with `./maze animation`.


### 2.checks whether each cell in the maze is accessible from the starting point

To check if all the cells are accessible it's possible to use some traversal algorithm.
To achieve that, I used the `Breadth-first search (BFS)` and `Depth-first search (DFS)` algorithms.
They're very similar and the major difference is that DFS uses a stack and BFS uses a queue to keep track of the cells.


### 3. shows the shortest route between starting point and destination
To show the shortest route, I used the A* algorithm which is awesome because it uses a heuristic to guide the searh and also takes into account the cost of the cell and the starting point.
A priority queue is used to avail the cells to be visited taking these costs into consideration

The heuristic used is the distance between the cell and the destination cell, which is an approximation since we don't know if there will be walls in front of us.
For that, the manhattan distance was used since we don't move in diagonals.


# Hints and Questions

### What is an efficient data structure to store the maze?
We can represent this maze as a graph, but it's a very specific graph because it always will have `m.n` (m is the width and n is the height) nodes and at most 4 edges (walls or connections) for each one.

The most common data structures to represent a graph are an adjacency matrix or adjacency list, so let's check the requirements for each one of these:

**Adjacency matrix**: Since a maze is a very sparse graph, we lose a lot of space representing empty edges. The space complexity is`O((m.n)^2)`.

**Adjacency list**: Adjacency lists are better because we would need to store at most 4 edges, so the space complexity is `O(m.n.4)`

**Prime factorization**  We can take advantage of the fact that that the edges can be only in four directions and we can use that in our favor.
Based on the [Gödel numbering for sequences](https://en.wikipedia.org/wiki/G%C3%B6del_numbering_for_sequences), we can use some primes to have a **single integer** to represent these connections, so the space complexity is `O(m.n)`

Lemme give you an example:
```
# We represent each direction as a different prime
SOUTH = 2
NORTH = 3
EAST = 5
WEST = 7

If we store the number 35 in the matrix position, that cell would have a connection with the west and east cells (7 x 5).
If we store the number 63 in the matrix position, that cell be connected with the south, north and west cells (7x3x2).
```

This is a very efficient data structure, but in programming, every decision is a tradeoff, so this is much more complex to implement and read.

Because of this, I implemented this behavior as a strategy that is encapsulated inside the `Maze` class

```
class Maze
  def initialize(width, height, strategy = PrimeFactorizationStrategy.new(width, height))
  end
```
That strategy is responsible to store the connections and later retrieve it to return the nighbors.
So, in that way, we're not tied to that specific implementation. In the future.

Check the `Maze` class for more details.


### In which context could this efficiency be defined?
This data structure is very efficient spacewise, but it's more expensive on the computation side.
Because, you need to calculate those directions.

If we had a adjacency list storing the cells, the program would use more memory, but it would be easier to read and maintain.
In summary, I would not go with this implementation unless we found that our adjacency lists are taking too much memory.

But this is a test and I implemented the most efficient data structure that I could think of.
So, why not?! =)

### Can you make sure that any randomly generated maze satisfies goal 2 (each cell is accessible)?
Yes!

We can take any maze and traverse the maze.
The number of visited cells should be equal to the width times height every time. 
If it's not, then there is at least one unreachable cells.

### How can a program that includes random functionality be efficiently tested?
It depends on the component that you're trying to test.
If it's a small component, like a number generator and we're unit testing the functionality, we could mock that and pass it to our SUT.

But, in my opinion, a good test is the one that tests the behavior, not the implementation.
For example, the tests for the two algorithms that randomly generates a new maze are being tested to check if all the paths can be reachable after its generation.
This guarantee at least a little safety on the implementation.

But, it's very hard to **really* guarantee that our software [will always work correctly](http://dilbert.com/strip/2001-10-25).
We could have a test suite that is executed a lot of times with different parameters.
When that suite breaks, we need to discover how that random component behaved.
Also, that suite could even schedule these tests in the CI.

# Bonus objectives - Provide a live visualization of how your algorithms work

To support this and to not couple the algorithm code with animation responsability, I used the observer pattern.
So, the algorithm simply calls `notify_visit(cell)` and the class that coordinates the animation observes and prints it accordingly.

I used it extensively in all the implemented algorithms.
